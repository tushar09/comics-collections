package tech.triumphit.comicscollection;

/**
 * Created by Tushar on 8/19/2016.
 */
public interface StartAnimation {
    void makeVisibleAndStartAnimation();
}
