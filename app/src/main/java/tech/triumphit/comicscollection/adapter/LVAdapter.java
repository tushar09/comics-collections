package tech.triumphit.comicscollection.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tech.triumphit.comicscollection.R;

/**
 * Created by Tushar on 8/6/2016.
 */
public class LVAdapter extends BaseAdapter {

    private final LayoutInflater inflater;
    ArrayList title, links, thumb, duration, views, likeanddislike;
    Context context;
    private int lastPosition = -1;
    public LVAdapter(Context context, ArrayList title, ArrayList links, ArrayList thumb, ArrayList duration, ArrayList views, ArrayList likeanddislike){

        this.context = context;

        this.title = title;
        this.links = links;
        this.thumb = thumb;
        this.duration = duration;
        this.views = views;
        this.likeanddislike = likeanddislike;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return links.size();
    }

    @Override
    public Object getItem(int position) {
        return links.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.lv_row, null);
            holder = new Holder();
            holder.image = (ImageView) convertView.findViewById(R.id.img);

            holder.title = (TextView) convertView.findViewById(R.id.textView4);
            holder.duration = (TextView) convertView.findViewById(R.id.textView5);
            holder.views = (TextView) convertView.findViewById(R.id.textView6);
            holder.likeanddislike = (TextView) convertView.findViewById(R.id.textView7);

            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }
        holder.title.setText("" + title.get(position));
        holder.duration.setText("" + duration.get(position));
        holder.views.setText("View: " + views.get(position));
        holder.likeanddislike.setText("" + likeanddislike.get(position));

        Picasso.with(context).load("" + thumb.get(position)).into(holder.image);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = YouTubeStandalonePlayer.createVideoIntent((Activity) context, "AIzaSyCH971kP9AOsKPB3aVPS7bbea1SG0vX3sY" , "" + links.get(position), 0,  true, false);
                //YouTubeStandalonePlayer.getReturnedInitializationResult(intent);
                ((Activity) context).startActivityForResult(intent, 5589);
            }
        });
        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;
        return convertView;
    }

    class Holder{
        TextView title, links, thumb, duration, views, likeanddislike;
        ImageView image;
    }
}
