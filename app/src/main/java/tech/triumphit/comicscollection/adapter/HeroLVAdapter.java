package tech.triumphit.comicscollection.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import tech.triumphit.comicscollection.MainActivity;
import tech.triumphit.comicscollection.R;
import tech.triumphit.comicscollection.Video;

/**
 * Created by Tushar on 8/15/2016.
 */
public class HeroLVAdapter extends BaseAdapter {

    ArrayList hero, thumb, category;
    Context context;
    private final LayoutInflater inflater;

    public HeroLVAdapter(Context context, ArrayList hero, ArrayList thumb, ArrayList category){
        this.context = context;
        this.hero = hero;
        this.thumb = thumb;
        this.category = category;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return hero.size();
    }

    @Override
    public Object getItem(int position) {
        return hero.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.lv_row_hero, null);
            holder = new Holder();
            holder.thumb = (ImageView) convertView.findViewById(R.id.img);
            holder.hero = (TextView) convertView.findViewById(R.id.textView24);
            holder.category = (TextView) convertView.findViewById(R.id.textView25);
            holder.fab = (FloatingActionButton) convertView.findViewById(R.id.fab);
            convertView.setTag(holder);
        }else{
            holder = (Holder) convertView.getTag();
        }

        holder.hero.setText("" + hero.get(position));
        holder.category.setText("" + category.get(position));
        Picasso.with(context).load("" + thumb.get(position)).fit().centerCrop().into(holder.thumb);

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity)context).setResult(23, new Intent(context, MainActivity.class).putExtra("hero", "" + hero.get(position)));
                ((Activity)context).finish();
            }

        });
        holder.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Video.class).putExtra("heroName", "" + hero.get(position)));
            }
        });

        return convertView;
    }

    private class Holder{
        ImageView thumb;
        TextView hero, category;
        FloatingActionButton fab;
    }

}
