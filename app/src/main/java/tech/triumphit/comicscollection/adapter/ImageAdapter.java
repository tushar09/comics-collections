package tech.triumphit.comicscollection.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import tech.triumphit.comicscollection.AnalyticsTrackers;
import tech.triumphit.comicscollection.MyService;
import tech.triumphit.comicscollection.R;
import tech.triumphit.comicscollection.Slider;

/**
 * Created by Tushar on 8/4/2016.
 */
public class ImageAdapter extends BaseAdapter {
    private final LayoutInflater inflater;
    private final SharedPreferences sp;
    private final SharedPreferences.Editor editor;
    int mGalleryItemBackground;
    private Context context;
    ArrayList thumbnail, link, title, size, page, hero, category;
    static Animation animation;
    private int lastPosition = -1;

    private final AnalyticsTrackers at;
    private final Tracker t;

    public ImageAdapter(Context c, ArrayList title, ArrayList thumbanil, ArrayList link, ArrayList size, ArrayList page, ArrayList hero, ArrayList category) {
        context = c;
        this.thumbnail = thumbanil;
        this.link = link;
        this.title = title;
        this.size = size;
        this.page = page;
        this.hero = hero;
        this.category = category;
//        TypedArray attr = mContext.obtainStyledAttributes(R.styleable.HelloGallery);
//        mGalleryItemBackground = attr.getResourceId(
//                R.styleable.HelloGallery_android_galleryItemBackground, 0);
//        attr.recycle();
        sp = context.getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);
    }

    public int getCount() {
        return thumbnail.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }
        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row, null);
            holder = new Holder();
            holder.thumbnail = (ImageView) convertView.findViewById(R.id.imageView);
            holder.title = (TextView) convertView.findViewById(R.id.textView);
            holder.page = (TextView) convertView.findViewById(R.id.textView3);
            holder.size = (TextView) convertView.findViewById(R.id.textView2);
            holder.hero = (TextView) convertView.findViewById(R.id.textView30);
            holder.category = (TextView) convertView.findViewById(R.id.textView31);
            //holder.title = (TextView) convertView.findViewById(R.id.title);
            convertView.setTag(holder);
        }else {
            holder = (Holder) convertView.getTag();
        }

        //holder.title.setText("" + title.get(position));
        Picasso.with(context).load("" + thumbnail.get(position)).fit().centerCrop().into(holder.thumbnail);
        holder.title.setText("" + title.get(position));
        holder.page.setText("" + page.get(position) + " pages");
        holder.size.setText("" + size.get(position) + "MB");
        holder.hero.setText("" + hero.get(position));
        holder.category.setText("" + category.get(position));

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                context.startActivity(new Intent(context, Slider.class).putExtra("link", "" + link.get(position)).putExtra("title", "" + title.get(position)));
                if(sp.getBoolean("music", true)){
                    context.startService(new Intent(context, MyService.class));
                }
                t.send(new HitBuilders.EventBuilder()
                        .setCategory("" + title.get(position))
                        .setAction(getDate() + " " + sp.getString("account", "notset"))
                        .setLabel(sp.getString("account", "notset"))
                        .build());

//                MediaPlayer mp;
//                Uri uri = Uri.parse("http://triumphit.tech/marvelVSdc/audio/Celtic%20Connections%20-%20Julie%20Fowlis.mp3");
//                mp = MediaPlayer.create(context, uri);
//                mp.start();
            }
        });

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        return convertView;

    }

    public String getDate() {
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy HH:mm a");
        String formattedDate = df.format(c.getTime());
        return formattedDate;
    }

    class Holder{
        TextView title, size, page, hero, category;
        ImageView thumbnail;
        CardView cv;
    }

}
