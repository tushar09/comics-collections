package tech.triumphit.comicscollection;

import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.startapp.android.publish.StartAppAd;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.regex.Pattern;

import tech.triumphit.comicscollection.adapter.LVAdapter;

public class Video extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    private ListView listView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList title, links, thumb, duration, views, likeanddislike;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        listView = (ListView) findViewById(R.id.lv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        loadVideo();
                                    }
                                }
        );

    }

    @Override
    public void onRefresh() {
        loadVideo();
    }

    public void loadVideo(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/marvelVSdc/video.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            title = new ArrayList();
                            links = new ArrayList();
                            thumb = new ArrayList();
                            duration = new ArrayList();
                            views = new ArrayList();
                            likeanddislike = new ArrayList();
                            JSONObject jo = new JSONObject(response);
                            JSONArray notice = jo.getJSONArray("video");
                            JSONObject jb = notice.getJSONObject(0);
                            String hero = "all";
                            if(!getIntent().getStringExtra("heroName").equals("all")){
                                hero = getIntent().getStringExtra("heroName").toLowerCase();
                                for (int i = 0; i < notice.length(); i++) {
                                    JSONObject jsonObject = notice.getJSONObject(i);
                                    if(("" + jsonObject.get("title")).toLowerCase().contains(hero.toLowerCase())){
                                        title.add(jsonObject.get("title"));
                                        links.add(jsonObject.get("link"));
                                        thumb.add(jsonObject.get("thumb"));
                                        duration.add(jsonObject.get("duration"));
                                        views.add(jsonObject.get("views"));
                                        likeanddislike.add(jsonObject.get("likeanddislike"));
                                    }
                                }
                            }else{
                                for (int i = 0; i < notice.length(); i++) {
                                    JSONObject jsonObject = notice.getJSONObject(i);
                                    title.add(jsonObject.get("title"));
                                    links.add(jsonObject.get("link"));
                                    thumb.add(jsonObject.get("thumb"));
                                    duration.add(jsonObject.get("duration"));
                                    views.add(jsonObject.get("views"));
                                    likeanddislike.add(jsonObject.get("likeanddislike"));

                                }
                            }

                            listView.setAdapter(new LVAdapter(Video.this, title, links, thumb, duration, views, likeanddislike));
                            swipeRefreshLayout.setRefreshing(false);
                        } catch (JSONException e1) {
                            Log.e("video error", e1.toString());
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 5589){
            startAppAd.showAd();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_video, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // Inflate the menu; this adds items to the action bar if it is present.
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //final int searchBarId = searchView.getContext().getResources().getIdentifier("android:id/search_bar", null, null);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 2000);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            Log.e("search erroe", e.toString());
        }

        MenuItem mi = menu.findItem(R.id.search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {

                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {

                        return true; // Return true to expand action view
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Search(newText);
                return true;
            }
        });


        return true;

    }

    private void Search(String newText) {
        ArrayList title, links, thumb, duration, views, likeanddislike;
        title = new ArrayList();
        links = new ArrayList();
        thumb = new ArrayList();
        duration = new ArrayList();
        views = new ArrayList();
        likeanddislike = new ArrayList();

        if (this.title != null && this.title.size() != 0) {
            for (int t = 0; t < this.title.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.title.get(t)).find()) {
                    title.add(this.title.get(t));
                    links.add(this.links.get(t));
                    thumb.add(this.thumb.get(t));
                    duration.add(this.duration.get(t));
                    views.add(this.views.get(t));
                    likeanddislike.add(this.likeanddislike.get(t));
                }
            }
        }
        listView.setAdapter(new LVAdapter(Video.this, title, links, thumb, duration, views, likeanddislike));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

}
