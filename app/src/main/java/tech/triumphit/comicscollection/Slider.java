package tech.triumphit.comicscollection;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Gallery;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.startapp.android.publish.StartAppAd;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Random;

import tech.triumphit.comicscollection.adapter.FullScreenImageAdapter;

public class Slider extends AppCompatActivity implements MediaPlayer.OnCompletionListener, StartAnimation{

    Gallery gallery;

    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    private StartAppAd startAppAd = new StartAppAd(this);
    ViewGroup viewGroup;
    boolean ful = false;
    boolean misc = false;
    boolean resume = false;
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;

    private TextView indicator;
    public static LinearLayout rel;

    public static Animation slide_up;
    public static Animation slide_down;
    public static Animation slide_in_right;
    public static Animation slide_out_right;
    private Handler handler;
    private Runnable runnable;
    private static boolean handlerCheck = false;
    private static SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slider);


        viewGroup = (ViewGroup) ((ViewGroup) this
                .findViewById(android.R.id.content)).getChildAt(0);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        rel = (LinearLayout) findViewById(R.id.linearLayout);
        seekBar = (SeekBar) findViewById(R.id.seekBar);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        counter = 0;


        indicator = (TextView) findViewById(R.id.textView27);

        slide_in_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_in_right);
        slide_out_right = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_out_right);

        slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up);
        slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_down);

        ful = sp.getBoolean("fullScreen", false);
        toggleFullScreen(sp.getBoolean("fullScreen", false));

        viewPager = (ViewPager) findViewById(R.id.pager);

        final FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ful) {
                    ful = false;
                    toggleFullScreen(ful);
                    //fab.setBackgroundResource(R.drawable.arrow_compress);
                    fab2.setImageDrawable(ContextCompat.getDrawable(Slider.this, R.drawable.arrow_expand));
                } else {
                    ful = true;
                    toggleFullScreen(ful);
                    //fab.setBackgroundDrawable(R.drawable.arrow_expand);
                    fab2.setImageDrawable(ContextCompat.getDrawable(Slider.this, R.drawable.arrow_compress));
                }


            }
        });
        final FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyService.mp.isPlaying()) {
                    MyService.mp.pause();
                    //fab.setBackgroundResource(R.drawable.arrow_compress);
                    fab1.setImageDrawable(ContextCompat.getDrawable(Slider.this, R.drawable.volume_high));
                } else {
                    MyService.mp.start();
                    //fab.setBackgroundDrawable(R.drawable.arrow_expand);
                    fab1.setImageDrawable(ContextCompat.getDrawable(Slider.this, R.drawable.volume_off));
                }

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);


        View checkBoxView = View.inflate(this, R.layout.checkbox, null);
        final CheckBox c = (CheckBox) checkBoxView.findViewById(R.id.checkBox5);
        //c.setText("Use it by default");
        c.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Log.e("check from dialog", "fasdf" + isChecked);
            }
        });

        String s = getIntent().getStringExtra("title");
        Log.e("class cast excp", sp.getString(s, "non"));

        resume = sp.getBoolean("showDialog", true);
        if(sp.getBoolean("showDialog", true)){
            if(sp.getString(s, "non").equals("yes")){
                builder.setTitle("Comics Collections")
                        .setMessage("Do you want to resume?")
                        .setView(checkBoxView)
                        .setPositiveButton("RESUME", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                resume = true;
                                new Loader().execute(getIntent().getStringExtra("link"));
                                if(c.isChecked()){
                                    editor.putBoolean("showDialog", true);
                                    editor.commit();
                                }
                            }
                        }).setNegativeButton("START OVER", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        resume = false;
                        editor.putBoolean("showDialog", false);
                        editor.commit();
                        new Loader().execute(getIntent().getStringExtra("link"));
                    }
                })
                        .setIcon(R.mipmap.ic_launcher);
                final AlertDialog a = builder.create();
                a.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                });
                a.show();

            }else{
                new Loader().execute(getIntent().getStringExtra("link"));
            }

        }else {
            new Loader().execute(getIntent().getStringExtra("link"));
        }
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                if(sp.getBoolean("hideController", false)){
                    update();
                }

                handler.postDelayed(this, 1000);
            }
        };
        if (!handlerCheck) {
            handler.postDelayed(runnable, 0);
            handlerCheck = true;
        }

    }
    public static int counter = 0;
    private void update() {
        counter++;
        if(counter == 8){
            rel.startAnimation(slide_out_right);
            seekBar.startAnimation(slide_down);
            slide_out_right.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rel.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            slide_down.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    seekBar.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        Random i;
        i = new Random();
        MyService.mp = mp;
        MyService.mp.release();
        int size = Containers.music.size();
        if(size == 0){
            size = 1;
        }
        int t = i.nextInt(size);
        if(t < 0){
            t = 0;
        }
        Uri uri = Uri.parse("" + Containers.music.get(t));
        Containers.music.remove(t);
        MyService.mp = MediaPlayer.create(Slider.this, uri);
        MyService.mp.start();
        MyService.mp.setOnCompletionListener(this);
        Log.e("here", "service here");
    }

    @Override
    public void makeVisibleAndStartAnimation() {
        rel.setVisibility(View.VISIBLE);
        seekBar.setVisibility(View.VISIBLE);
        seekBar.startAnimation(slide_up);
        counter = 0;
        rel.startAnimation(slide_in_right);
        slide_in_right.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private class Loader extends AsyncTask<String, String, String> {

        ArrayList img = new ArrayList();

        @Override
        protected String doInBackground(String[] params) {
            Document doc = null;
            try {
                doc = Jsoup.connect(params[0])
                        .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750.152 Safari/537.36")
                        .get();
                Elements media = doc.select("[src]");
                for (Element src : media) {
                    if (src.tagName().equals("img")) {
                        String s = ("" + src.attr("abs:src"));
                        s = s.substring(0, s.lastIndexOf("?"));
                        if(sp.getBoolean("qualityPic", false)){
                            s = s + "?w=500";
                        }
                        img.add(s);
                    }
                }
                //Collections.sort(img);
            } catch (IOException e) {
                Log.e("Error", e.toString());
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //gallery.setAdapter(new GalleryAdapter(Slider.this, img));

            seekBar.setMax(img.size());
            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

                boolean b = false;

                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    b = fromUser;
                    if(fromUser){
                        indicator.setText(progress + "/" + img.size());
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if(b){
                        viewPager.setCurrentItem(seekBar.getProgress());
                    }
                    indicator.setText(seekBar.getProgress() + "/" + img.size());
                }
            });

            adapter = new FullScreenImageAdapter(Slider.this, img);
            viewPager.setAdapter(adapter);
            viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    Log.e("selected", "" + position);
                    indicator.setText(position + "/" + img.size());
                    editor.putInt(getIntent().getStringExtra("title") + "2", position);
                    editor.commit();
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });
            viewPager.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
            if(resume){
                //reduceLink(img);
                viewPager.setCurrentItem(sp.getInt(getIntent().getStringExtra("title") + "2", 0));
                seekBar.setProgress(sp.getInt(getIntent().getStringExtra("title") + "2", 0));
            }else{

            }
            //startService(new Intent(Slider.this, MyService.class));
        }
    }

    private void reduceLink(ArrayList img) {
        int length = sp.getInt(getIntent().getStringExtra("title"), 0);
        for (int i = 0; i < length; i++) {
            img.remove(0);

        }
    }

    @Override
    public void onBackPressed() {
        startAppAd.showAd();
        stopService(new Intent(this, MyService.class));
        if(MyService.mp != null) {
            if (MyService.mp.isPlaying()) {
                MyService.mp.stop();
                MyService.mp.release();
            }
        }
        //editor.putString(getIntent().getStringExtra("title"), );
        super.onBackPressed();
    }

    void toggleFullScreen(boolean goFullScreen) {
        if (goFullScreen) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
        } else {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        viewGroup.requestLayout();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        editor.putString(getIntent().getStringExtra("title"), "yes");
        editor.commit();
    }
}
