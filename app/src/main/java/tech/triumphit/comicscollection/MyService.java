package tech.triumphit.comicscollection;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;

import java.util.Random;

public class MyService extends Service {
    public static MediaPlayer mp = null;
    public static Random i;

    private static Slider slider;
    private static StartAnimationListener startAnimationListener;

    public MyService() {
        mp = new MediaPlayer();
        i = new Random();
        slider = new Slider();
        startAnimationListener = new StartAnimationListener();
        startAnimationListener.receiveTriger(slider);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        if (mp == null) {
            int size = Containers.music.size();
            if(size == 0){
                size = 1;
            }
            int t = i.nextInt(size);
            if(t < 0){
                t = 0;
            }
            if(Containers.music.size() != 0){
                Uri uri = Uri.parse("" + Containers.music.get(t));
                Containers.music.remove(t);
                mp = MediaPlayer.create(this, uri);
                mp.start();
            }

        }else{
            mp.release();
            mp = null;
            int size = Containers.music.size();
            if(size == 0){
                size = 1;
            }
            int t = i.nextInt(size);
            if(t < 0){
                t = 0;
            }
            if(Containers.music.size() != 0){
                Uri uri = Uri.parse("" + Containers.music.get(t));
                Containers.music.remove(t);
                mp = MediaPlayer.create(this, uri);
                mp.start();
            }

            //mp.start();

        }

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

}
