package tech.triumphit.comicscollection;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.animation.Animator;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.squareup.picasso.Picasso;
import com.startapp.android.publish.StartAppAd;
import com.startapp.android.publish.StartAppSDK;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import tech.triumphit.comicscollection.adapter.ImageAdapter;

public class MainActivity extends AppCompatActivity {

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private ListView gv;
    private SwipeRefreshLayout swipeRefreshLayout;
    private LinearLayout mLinearScroll;
    SharedPreferences sp;
    SharedPreferences.Editor editor;
    private Account account;
    private String accountName;
    private AnalyticsTrackers at;
    private Tracker t;

    ArrayList name, link, cover, category;
    private ArrayList id;
    private ArrayList size;
    private ArrayList page, hero;
    private ViewFlipper iv;
    private ImageView icon;
    ImageView v1, v2, v3, v4;
    private TextView titleText, subTitle;
    private StartAppAd startAppAd = new StartAppAd(this);
    private int totalNumberOfMovie = 0;
    int s = 0;
    int e = 29;
    Button previousClicked;
    private TextView home, noticeText;

    private BroadcastReceiver mRegistrationBroadcastReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");

        StartAppSDK.init(this, "207540149");

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check type of intent filter
                if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_SUCCESS)){
                    //Registration success
                    String token = intent.getStringExtra("token");

                } else if(intent.getAction().equals(GCMRegistrationIntentService.REGISTRATION_ERROR)){
                    //Registration error

                } else {
                    //Tobe define
                }
            }
        };
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if(ConnectionResult.SUCCESS != resultCode) {
            //Check type of error
            if(GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                Toast.makeText(getApplicationContext(), "Google Play Service is not install/enabled in this device!", Toast.LENGTH_LONG).show();
                //So notification
                GooglePlayServicesUtil.showErrorNotification(resultCode, getApplicationContext());
            } else {
                Toast.makeText(getApplicationContext(), "This device does not support for Google Play Service!", Toast.LENGTH_LONG).show();
            }
        } else {
            //Start service
            Intent itent = new Intent(this, GCMRegistrationIntentService.class);
            startService(itent);
        }

        initializeAllApi();
        home = (TextView) toolbar.findViewById(R.id.textView45);
        noticeText = (TextView) toolbar.findViewById(R.id.textView46);

        gv = (ListView) findViewById(R.id.gridView);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);

        View headerview = mNavigationView.getHeaderView(0);
        iv = (ViewFlipper) headerview.findViewById(R.id.imageSwitcher);
        icon = (ImageView) headerview.findViewById(R.id.profile_image);
        Picasso.with(this).load(Containers.ad[3]).fit().centerCrop().into(icon);
        v1 = (ImageView) iv.findViewById(R.id.v1);
        v2 = (ImageView) iv.findViewById(R.id.v2);
        v3 = (ImageView) iv.findViewById(R.id.v3);
        v4 = (ImageView) iv.findViewById(R.id.v4);
//
        Picasso.with(this).load(Containers.ad[4]).fit().centerCrop().into(v1);
        Picasso.with(this).load(Containers.ad[5]).fit().centerCrop().into(v2);
        Picasso.with(this).load(Containers.ad[6]).fit().centerCrop().into(v3);
        Picasso.with(this).load(Containers.ad[7]).fit().centerCrop().into(v4);

        titleText = (TextView) headerview.findViewById(R.id.username);
        subTitle = (TextView) headerview.findViewById(R.id.email);

        titleText.setText(Containers.ad[1]);
        subTitle.setText(Containers.ad[2]);

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Containers.ad[8]));
                startActivity(browserIntent);
//                t.send(new HitBuilders.EventBuilder()
//                        .setCategory("Ad " + Containers.ad[8])
//                        .setAction("ad Clicked " + sp.getString("account", "notset"))
//                        .setLabel("Clicked Full " + sp.getString("account", "notset") + " " + getDeviceID())
//                        .build());
//                sendReport();
            }
        });



        final Animation in = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        final Animation out = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
        iv.setInAnimation(in);
        iv.setOutAnimation(out);
        iv.setFlipInterval(7500);
        iv.startFlipping();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if(item.getItemId() == R.id.setting){
                    startActivity(new Intent(MainActivity.this, Setting.class));
                }
                if(item.getItemId() == R.id.hero){
                    startActivityForResult(new Intent(MainActivity.this, Heroes.class), 23);
                    mDrawerLayout.closeDrawers();
                }
                if(item.getItemId() == R.id.videos){
                    startActivity(new Intent(MainActivity.this, Video.class).putExtra("heroName", "all"));
                    mDrawerLayout.closeDrawers();
                }
                if(item.getItemId() == R.id.about){
                    startActivity(new Intent(MainActivity.this, About.class));
                    mDrawerLayout.closeDrawers();
                }
                if(item.getItemId() == R.id.marvel){
                    s = 0;
                    e = 0;
                    setCategory(s, e, "marvel");
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("MARVEL")
                            .setAction("MARVEL Clicked " + sp.getString("account", "notset"))
                            .setLabel("MARVEL Clicked " + sp.getString("account", "notset"))
                            .build());
                    mDrawerLayout.closeDrawers();
                }
                if(item.getItemId() == R.id.dc){
                    s = 0;
                    e = 0;
                    setCategory(s, e, "dc");
                    t.send(new HitBuilders.EventBuilder()
                            .setCategory("DC")
                            .setAction("DC Clicked " + sp.getString("account", "notset"))
                            .setLabel("DC Clicked " + sp.getString("account", "notset"))
                            .build());
                    mDrawerLayout.closeDrawers();
                }
                if(item.getItemId() == R.id.review){
                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                    try {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                        t.send(new HitBuilders.EventBuilder()
                                .setCategory("Rate Clicked")
                                .setAction("Rate Clicked " + sp.getString("account", "notset"))
                                .setLabel("Rate Clicked " + sp.getString("account", "notset"))
                                .build());
                    } catch (android.content.ActivityNotFoundException anfe) {
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                    }
                }
                return false;
            }
        });
        mLinearScroll = (LinearLayout) findViewById(R.id.linear_scroll);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (home.getText().toString().equals("Home")) {
                    loadData(s, e);
                } else {
                    sort(s, e, home.getText().toString().toLowerCase());
                }
            }
        });
        final ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();
        mNavigationView.setItemIconTintList(null);

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        loadData(s, e);
                                    }
                                }
        );

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("dd");
            String formattedDate = df.format(c.getTime());
            int date = Integer.parseInt(formattedDate);
            if (date % sp.getInt("reviewCycle", 3) == 0) {
                if (sp.getBoolean("review", true)) {
                    editor.putBoolean("review", false);
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);

                    builder.setTitle("Review Comics Collections")
                            .setMessage("Please rate this app. Your review will help us to improve our Comics Collection.")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                    try {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                    } catch (android.content.ActivityNotFoundException anfe) {
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("amzn://apps/android?p=" + appPackageName)));
                                        //String url = "http://www.amazon.com/TriumphIT-Music-Player/dp/B00ZD2D1T2/ref=sr_1_16?s=mobile-apps&ie=UTF8&qid=1434885049&sr=1-16&keywords=amazon+mp3&pebp=1434885051870&perid=10M3Z3SR7RE5RBMZ1M1N";
                                        //Intent i = new Intent(Intent.ACTION_VIEW);
                                        //i.setData(Uri.parse(url));
                                        //startActivity(i);
                                    }
                                    editor.putInt("reviewCycle", 30);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            })
                            .setNegativeButton("Never", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    editor.putInt("reviewCycle", 20);// do nothing
                                    editor.commit();
                                    finish();
                                }
                            }).setNeutralButton("Later", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            editor.putInt("reviewCycle", 3);// do nothing
                            editor.commit();
                            finish();
                        }
                    })
                            .setIcon(R.mipmap.ic_launcher);

                    final AlertDialog a = builder.create();
                    a.setOnShowListener(new DialogInterface.OnShowListener() {
                        @Override
                        public void onShow(DialogInterface dialog) {
                            a.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(getResources().getColor(R.color.colorPrimary));
                            a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    });
                    a.show();
                    editor.commit();

                } else {
                    finish();
                    startAppAd.showAd();
                }
                //moveTaskToBack(false);

                return true;
            } else {
                editor.putBoolean("review", true);
                editor.commit();
                startAppAd.showAd();
            }
            //onBackPressed();

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 23){
            if(data != null){
                hero(s, e, data.getStringExtra("hero"));
            }
            //Log.e("result test", data.getStringExtra("hero"));
        }
    }

    private void hero(final int s, final int e, final String hero) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/marvelVSdc/sortHero.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullComicInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("hero", hero);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setCategory(final int s, final int e, final String cat) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/marvelVSdc/category.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullComicInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("c", cat);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void loadData(final int s, final int e) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/marvelVSdc/comics.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullComicInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setReadyPagination(int button) {
        if (name != null && name.size() != 0) {
            //int size = (totalNumberOfMovie / 30) + 1;

            for (int j = 0; j < button; j++) {
                final int k;
                k = j;
                final Button btnPage = new Button(MainActivity.this);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(120,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0, 0, 0, 0);
                btnPage.setTextColor(Color.WHITE);
                btnPage.setTextSize(13.0f);
                btnPage.setId(j);
                btnPage.setText(String.valueOf(mLinearScroll.getChildCount() + 1));
                btnPage.setBackgroundResource(R.drawable.button_unfocused);
                if (j == 0) {
                    previousClicked = btnPage;
                    btnPage.setBackgroundResource(R.drawable.button_focus);
                    previousClicked.setBackgroundResource(R.drawable.button_focus);
                    btnPage.setTextColor(Color.parseColor("#000000"));
                }
                mLinearScroll.addView(btnPage, lp);


                btnPage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        previousClicked.setBackgroundResource(R.drawable.pagebuttontranssss);
                        previousClicked.setTextColor(Color.parseColor("#ffffff"));

                        btnPage.setBackgroundResource(R.drawable.pagebuttontrans);
                        btnPage.setTextColor(Color.parseColor("#000000"));
                        TransitionDrawable transition = (TransitionDrawable) btnPage.getBackground();
                        transition.startTransition(250);
                        TransitionDrawable transition2 = (TransitionDrawable) previousClicked.getBackground();
                        transition2.startTransition(250);
                        previousClicked = btnPage;
                        s = (30 * btnPage.getId());
                        e = ((30 * (btnPage.getId() + 1)) - 1);
                        Log.e("adfdsafadsfasdf", e + "");
                        //onRefresh = "http://android.teknobilim.org/Movie/?user=MoVieS&pass=58a048f77d38a0ced7321abaf96fdf30&i=" + (30 * Integer.parseInt("" + btnPage.getId())) + "&f=" + ((30 * Integer.parseInt("" + (btnPage.getId() + 1))) - 1) + "&id";
                        //new PullMovieInfo().execute(onRefresh);
                        if (home.getText().toString().equals("Home")) {
                            loadData(s, e);
                        } else {
                            sort(s, e, home.getText().toString().toLowerCase());
                        }

                    }
                });
            }
        }
    }
    private class PullComicInfo extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {

            name = new ArrayList();
            link = new ArrayList();
            cover = new ArrayList();
            id = new ArrayList();
            size = new ArrayList();
            page = new ArrayList();
            hero = new ArrayList();
            category = new ArrayList();

            try {

                JSONObject jo = new JSONObject(params[0]);
                JSONArray notice = jo.getJSONArray("FullComics");

                JSONArray totalItems = jo.getJSONArray("Total");
                JSONObject total = totalItems.getJSONObject(0);
                totalNumberOfMovie = total.getInt("total");
                Log.e("total", "" + totalNumberOfMovie);

                for (int i = 0; i < notice.length(); i++) {
                    JSONObject jsonObject = notice.getJSONObject(i);
                    name.add(jsonObject.get("name"));
                    link.add(jsonObject.get("link"));
                    Log.e("link", "" + link.get(i));
                    cover.add(jsonObject.get("cover"));
                    size.add(jsonObject.get("size"));
                    page.add(jsonObject.get("page"));
                    hero.add(jsonObject.get("hero"));
                    category.add(jsonObject.get("category"));
                }


            } catch (JSONException e1) {
                Log.e("total", e1.toString());

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if(gv != null){
                gv.setAdapter(new ImageAdapter(MainActivity.this, name, cover, link, size, page, hero, category));
            }

            int button = totalNumberOfMovie / 30;
            if (totalNumberOfMovie % 30 != 0) {
                button++;
            }
            Log.e("button", "" + button);
            button = button - mLinearScroll.getChildCount();
            setReadyPagination(button);
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void initializeAllApi() {

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        account = getAccount(AccountManager.get(getApplicationContext()));
        if (account == null) {
            accountName = "notSet";
            editor.putString("account", accountName);
            editor.commit();
        } else {
            accountName = account.name;
            editor.putString("account", accountName);
            editor.commit();
        }

//        Pushbots.sharedInstance().init(this);
//        Pushbots.sharedInstance().setAlias(accountName + " " + Build.DEVICE + ": " + Build.MODEL);

        StartAppSDK.init(this, "207540149", true);

        AnalyticsTrackers.initialize(this);
        at = AnalyticsTrackers.getInstance();
        t = at.get(AnalyticsTrackers.Target.APP);

    }

    public static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        // Inflate the menu; this adds items to the action bar if it is present.
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        //final int searchBarId = searchView.getContext().getResources().getIdentifier("android:id/search_bar", null, null);
        LinearLayout searchBar = (LinearLayout) searchView.findViewById(R.id.search_bar);
        LayoutTransition transitioner = new LayoutTransition();
        Animator appearingAnimation = ObjectAnimator.ofFloat(null, "translationX", 600, 0);
        transitioner.setAnimator(LayoutTransition.APPEARING, appearingAnimation);
        transitioner.setDuration(LayoutTransition.APPEARING, 2000);
        transitioner.setStartDelay(LayoutTransition.APPEARING, 0);
        searchBar.setLayoutTransition(transitioner);

        AutoCompleteTextView searchTextView = (AutoCompleteTextView) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        try {
            Field mCursorDrawableRes = TextView.class.getDeclaredField("mCursorDrawableRes");
            mCursorDrawableRes.setAccessible(true);
            mCursorDrawableRes.set(searchTextView, R.drawable.cursor); //This sets the cursor resource ID to 0 or @null which will make it visible on white background
        } catch (Exception e) {
            Log.e("search erroe", e.toString());
        }

        MenuItem mi = menu.findItem(R.id.search);
        MenuItemCompat.setOnActionExpandListener(mi,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {

                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        loadData(0, totalNumberOfMovie);
                        return true; // Return true to expand action view
                    }
                });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Search(newText);
                return true;
            }
        });


        return true;

    }

    private void Search(String newText) {

        ArrayList name, link, cover;
        ArrayList id;
        ArrayList size;
        ArrayList page, hero, category;
        name = new ArrayList();
        link = new ArrayList();
        cover = new ArrayList();
        size = new ArrayList();
        page = new ArrayList();
        hero = new ArrayList();
        category = new ArrayList();
        if (this.name != null && this.name.size() != 0) {
            for (int t = 0; t < this.name.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.name.get(t)).find()) {
                    name.add(this.name.get(t));
                    size.add(this.size.get(t));
                    link.add(this.link.get(t));
                    cover.add(this.cover.get(t));
                    page.add(this.page.get(t));
                    hero.add(this.hero.get(t));
                    category.add(this.category.get(t));
                }
            }
        }
        gv.setAdapter(new ImageAdapter(MainActivity.this, name, cover, link, size, page, hero, category));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if(id == R.id.home){
            home.setText("Home");
            loadData(s, e);
        }
        if(id == R.id.name){
            home.setText("Name");
            sort(s, e, "name");
        }
        if(id == R.id.hero){
            home.setText("Hero");
            sort(s, e, "hero");
        }
        if(id == R.id.page){
            home.setText("Page");
            sort(s, e, "page");
        }
        if(id == R.id.size){
            home.setText("Size");
            sort(s, e, "size");
        }

        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    private void sort(final int s, final int e, final String category) {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://triumphit.tech/marvelVSdc/sort.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        new PullComicInfo().execute(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams(){
                Map<String, String> params = new HashMap<String, String>();
                params.put("s", "" + s);
                params.put("e", "" + e);
                params.put("c", category);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        startAppAd.onBackPressed();
        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.w("MainActivity", "onResume");
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_SUCCESS));
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(GCMRegistrationIntentService.REGISTRATION_ERROR));
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.w("MainActivity", "onPause");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
    }
}
