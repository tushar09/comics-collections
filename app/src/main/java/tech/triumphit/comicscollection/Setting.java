package tech.triumphit.comicscollection;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.startapp.android.publish.StartAppAd;

public class Setting extends AppCompatActivity implements CheckBox.OnCheckedChangeListener{

    CheckBox music, data, fulscreen, controller;
    private SharedPreferences sp;
    private SharedPreferences.Editor editor;
    private StartAppAd startAppAd = new StartAppAd(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sp = getSharedPreferences("utils", Context.MODE_PRIVATE);
        editor = sp.edit();

        music = (CheckBox) findViewById(R.id.checkBox);
        data = (CheckBox) findViewById(R.id.checkBox2);
        fulscreen = (CheckBox) findViewById(R.id.checkBox3);
        controller = (CheckBox) findViewById(R.id.checkBox4);

        music.setOnCheckedChangeListener(this);
        data.setOnCheckedChangeListener(this);
        fulscreen.setOnCheckedChangeListener(this);
        controller.setOnCheckedChangeListener(this);

        music.setChecked(sp.getBoolean("music", true));
        data.setChecked(sp.getBoolean("qualityPic", false));
        fulscreen.setChecked(sp.getBoolean("fullScreen", false));
        controller.setChecked(sp.getBoolean("hideController", false));

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(buttonView.getId() == R.id.checkBox){
            editor.putBoolean("music", isChecked);
            Log.e("music",  "" + isChecked);
        }
        if(buttonView.getId() == R.id.checkBox2){
            editor.putBoolean("qualityPic", isChecked);
            Log.e("quality",  "" + isChecked);
        }
        if(buttonView.getId() == R.id.checkBox3){
            editor.putBoolean("fullScreen", isChecked);
            Log.e("fullScreen",  "" + isChecked);
        }
        if(buttonView.getId() == R.id.checkBox4){
            editor.putBoolean("hideController", isChecked);
            Log.e("hideController",  "" + isChecked);
        }
        editor.commit();
    }

    @Override
    public void onBackPressed() {
        startAppAd.showAd();
        super.onBackPressed();
    }
}
