package tech.triumphit.comicscollection;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                checkMaintenance();
            }
        }, 3000);
    }

    void checkMaintenance() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/marvelVSdc/maintenance.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("maintenance");
                            JSONObject mode = jArr.getJSONObject(0);
                            String maintenanceMode = mode.getString("maintenancemode");
                            if(maintenanceMode.equals("yes")){
                                AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                                builder.setTitle("Comics Collections")
                                        .setMessage("Sorry, server under maintenance. Please wait for 1 hour.")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        })
                                        .setIcon(R.mipmap.ic_launcher);
                                final AlertDialog a = builder.create();
                                a.setOnShowListener(new DialogInterface.OnShowListener() {
                                    @Override
                                    public void onShow(DialogInterface dialog) {
                                        a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                                    }
                                });
                                a.show();
                                //Log.e("maintenance", maintenanceMode + " from run");
                            }else if(maintenanceMode.equals("no")){
                                getAds();

                            }
                            //("maintenance", maintenanceMode);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Splash.this);
                        builder.setTitle("Comics Collections")
                                .setMessage("Sorry, could not connect to the Internet. Please restart the app")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher);
                        final AlertDialog a = builder.create();
                        a.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface dialog) {
                                a.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.colorPrimary));
                            }
                        });
                        a.show();
                        //dfinish();
                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getAds() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/funnyvideos/ad/ad.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("maintenance", response + "sdfgdfgsdg");
                        JSONObject jsonObject1 = null;
                        try {
                            jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("ad");
                            JSONObject jsonObject = jArr.getJSONObject(0);
                            Containers.ad [0] = "" + jsonObject.get("id");
                            Containers.ad [1] = "" + jsonObject.get("title");
                            Containers.ad [2] = "" + jsonObject.get("subtitle");
                            Containers.ad [3] = "" + jsonObject.get("icon");
                            Containers.ad [4] = "" + jsonObject.get("pic1");
                            Containers.ad [5] = "" + jsonObject.get("pic2");
                            Containers.ad [6] = "" + jsonObject.get("pic3");
                            Containers.ad [7] = "" + jsonObject.get("pic4");
                            Containers.ad [8] = "" + jsonObject.get("action");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        getMusic();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void getMusic() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/marvelVSdc/music.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Log.e("maintenance", response + "sdfgdfgsdg");
                        JSONObject jsonObject1 = null;
                        try {
                            jsonObject1 = new JSONObject(response);
                            JSONArray jArr = jsonObject1.getJSONArray("Music");
                            for(int t = 0; t < jArr.length(); t++){
                                JSONObject jsonObject = jArr.getJSONObject(t);
                                Containers.music.add("" + jsonObject.get("link"));
                            }

                            //Log.e("music", "" + jsonObject.get("link"));
                        } catch (JSONException e) {
                            Log.e("music", "" + e.toString());
                            e.printStackTrace();
                        }

                        for (int i = 0; i < Containers.music.size(); i++) {
                            Log.e("music", "" + Containers.music.get(i));
                        }

                        Intent i = new Intent(Splash.this, MainActivity.class);
                        startActivity(i);
                        finish();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
