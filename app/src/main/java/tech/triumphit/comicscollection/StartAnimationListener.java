package tech.triumphit.comicscollection;

/**
 * Created by Tushar on 8/19/2016.
 */
public class StartAnimationListener {

    StartAnimation startAnimation;

    public void receiveTriger(StartAnimation startAnimation){
        this.startAnimation = startAnimation;
    }

    public void shootEvent(){
        startAnimation.makeVisibleAndStartAnimation();
    }
}
