package tech.triumphit.comicscollection;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import tech.triumphit.comicscollection.adapter.HeroLVAdapter;

public class Heroes extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{

    ListView lv;
    SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList hero, category, thumb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heroes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lv);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        loadData();
                                    }
                                }
        );
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    private void loadData() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://triumphit.tech/marvelVSdc/heroes.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hero = new ArrayList();
                        category = new ArrayList();
                        thumb = new ArrayList();
                        try {

                            JSONObject jo = new JSONObject(response);
                            JSONArray notice = jo.getJSONArray("FullComics");

                            for (int i = 0; i < notice.length(); i++) {
                                JSONObject jsonObject = notice.getJSONObject(i);
                                hero.add(jsonObject.get("hero"));
                                category.add(jsonObject.get("category"));
                                thumb.add(jsonObject.get("cover"));
                            }
                            lv.setAdapter(new HeroLVAdapter(Heroes.this, hero, thumb, category));
                            swipeRefreshLayout.setRefreshing(false);

                        } catch (JSONException e1) {
                            Log.e("total", e1.toString());
                            Toast.makeText(Heroes.this, "Something went wrong, Please try again.", Toast.LENGTH_SHORT).show();
                            swipeRefreshLayout.setRefreshing(false);
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
